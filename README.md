<!--<a href="http://gambaswiki.org/wiki/screenshot/cygwin-ide.png?v"><img src="http://gambaswiki.org/wiki/screenshot/t-cygwin-ide.png?v"></a>&nbsp;&nbsp;&nbsp;&nbsp;
<a href="http://gambaswiki.org/wiki/screenshot/webform-ide.png?v"><img src="http://gambaswiki.org/wiki/screenshot/t-webform-ide.png?v"></a>&nbsp;&nbsp;&nbsp;&nbsp;-->

### Please Note.
This is an alternative "Branch" forked from the main Gambas3 repository.  [gambas @ gitlab](https://gitlab.com/gambas/gambas)\
It is a copy of the official master Gambas3 version but has a few extras added to the IDE...\

> <u>All modifications (except the Makefile speedup) are to the IDE only and not the components or internal classes making produced programs compatible with other gambas systems only the development experience is different</u>


**An incomplete list of the patches in this branch...**\

* <a href="https://gitlab.com/bsteers4/gambas/-/commit/40224153abecd46e7782e37002f2e2f0db70abe8">Makefile speedup: "make install" only installs mimetypes if not already installed.</a>\
    Some systems pause for a while during install when installing mimetypes.\
    This skips all 3 mimetype installs if not needed.\
    (Does not skip on OpenSuse or Fedora as xdg-mime cannot determine filetypes as root on these systems)

* <a href="https://gitlab.com/bsteers4/gambas/-/commit/8138496d3bf8917d88a574b32a0a7cdd21c05671">Show "Trunk version" on Welcome screen.</a>\
    This displays the Trunk version (commit number) in the welcome screen.\
    Usefull for those helping with gambas development branch.

* <a href="https://gitlab.com/bsteers4/gambas/-/commit/5a541e1a72ad93b37d18cf9a8b88c3fd3684b209">Enable Quote wrapping if Alt key is pressed.</a>\
    This will wrap text in quotes (like braces) but only when presisng alt key.\
    (this simply unquotes Benoits original code he disabled but adds an "If Key.Alt" to the condition)

* Alternative Property completion.\
  If you type something like 'Property Text As String' then hit return the Text_Read and Text_Write are automatically added.
  Pressing Alt-Return will now add a 'Private $sText As String' definition and use $sText for the Read/Write methods.

* Festive sound plays only once.\
    Times like Christmas gambas will play bell sounds every time you load it. it's nice but gets annoying if you use your computer to also play your music and load gambas many times. This mod makes a note of the month when it plays and will not play again if it already has.

* Desktop shortcut mod.\
  When making an executable you can select to also make a desktop icon. 
    Now you can also select to add the Actions for running in gtk2/gtk3/qt4/qt5 with applications using gb.gui and an "Edit in gambas IDE" action that will load the project into gambas for you 

* Make Executable has options to run a command both before and after making the executable.

* Option to hold search/replace window open when searching.

* Copying the executable filename to the clipboard when making an exe is an Option not a default action.

* Added a couple of args to gambas3 for shell use...\
  * **gambas3 --makearchive (or --makearchivegmail) \<project directory> \<archive path>**\
    Makes a project source archive, if archive path is not given the make archive dialog opens.\
    (use --makearchivegmail to use the fool gmail feature.)

  * **gambas3 --makeexe (or --makeexeclean) \<project directory>**\
    Recompile a project source , if exe does not exist or a filename not set the make exe dialog opens.\
    Same as pressing "Make Executable" in the IDE.\
    (Use --makeexeclean to compile all sources.)


* Added Bonus tweaks (developer options) page to preferences where many of my tweaks can be enabled disabled.

* Custom Controls (use with caution)\
    You can add custom controls / components that will load with the IDE. this makes the controls render properly in the form designer instead of just showing a box with it's name.\
    This feature can be tricky if your coding is not strong and could cause minor problems with your gambas IDE.\
    (Usually the problem is the IDE has initially loaded a control that you are working on and it has now changed. (the IDE cannot reload a control) 
    This is simply fixed by restarting the IDE and the modified control will then be loaded, once you have finished making your control it should work with no problems)

* External Tools (WIP)\
    Works like Pluma external tools.\
    You can create your own **very handy** shell scripts to do anything you want (bash or gambas scripts) and pass various parameters to process selected text or entire document contents.

    The results can be inserted at cursor position or even replace the whole document.\
    The External tools selector/editor can be found in the advanced edit menu or by pressing Ctrl-E
    There is a problem with creating files :(, sometimes (not always) a new script after writing it saves without the edits (annoying, sorry) problem seems only occasional so i work around it until i can work out the bug.

* Inhibit shutdown.\
  Works on MATE and some gnome based systems, uses the org.gnome.SessionManager DBus interface to inhibit shutdown/logout if unsaved documents are open.

* Hide extentions in tabs.\
  Only shows the files BaseName not the extension in the IDE tabs so more tabs can be seen on the screen.

* Keystroke recorder\
  The IDE text editor now has a way to record your keystrokes and then play them back. This can be very usefull for adjusting many lines of code a specific way.\
  Copy and Paste are supported keystrokes as well as F3 to search for a string occurence.

Much respect to Benoit Minisini and all others involved for the wonderfully configurable gambas environment.

Bruce Steers

<hr width=75%>
<hr width=50%>
<hr width=25%>

### Below is the original README.md


<a href="http://gambas.sf.net/2014-07-26.png"><img src="http://gambas.sf.net/t-2014-07-26.png"></a>
![The Gambas development environment](https://gambaswiki.org/wiki/screenshot/2025-01-10.png)

[![Please don't upload to GitHub](https://nogithub.codeberg.page/badge.svg)](https://nogithub.codeberg.page)

# Gambas Almost Means BASIC

WELCOME TO GAMBAS!

GAMBAS is a free implementation of a graphical development environment 
based on a BASIC interpreter and a full development platform. It is very 
inspired by Visual Basic and Java.

Go to http://gambas.sourceforge.net to get more information: how to compile 
and install it, where to find binary packages, how to report a bug...

Go to http://gambaswiki.org for language documentation.

The following pieces of code were borrowed and adapted:

- The natural string comparison algorithme was adapted from the algorithm 
  made by Martin Pol. See http://sourcefrog.net/projects/natsort/ for more 
  details.

- The hash table implementation was adapted from the glib one.

- The HTML entities parsing in 'gb.gtk' comes from KHTML sources.

- The 'gb.image.effect' sources are adapted from KDE 3 image effect routines.

- The 'gb.clipper' library embeds the Clipper library. See
  http://www.angusj.com/delphi/clipper.php for mode details.

- The function that computes the easter day of a specific year uses an
  algorithm made by Aloysius Lilius And Christophorus Clavius.

- The blurring algoritm is based on the 'StackBlur' algorithm made by Mario 
  Klingemann. See http://incubator.quasimondo.com/processing/fast_blur_deluxe.php 
  for more details.
  
- The javascript automatic completion is done with 'autoComplete' from Simon 
  Steinberger / Pixabay, and is published under the MIT license.
  See https://github.com/Pixabay/JavaScript-autoComplete for more details.

- The 'gb.form.htmlview' component embeds the 'litehtml' library from Yuri 
  Kobets. See http://www.litehtml.com for more details.

- The 'gb.hash' component embeds the code of the hashing routines of BusyBox made
  by Denys Vlasenko and Bernhard Reutner-Fischer. See https://www.busybox.net/
  for more details.

- The stock icons flags were generated from SVG files taken from
  https://github.com/smucode/react-world-flags. They are published under the MIT
  license.
  
- The algorithm of the [Paint.SmoothPolyline()](https://gambaswiki.org/wiki/comp/gb.qt4/paint/smoothpolyline)
  method is based on https://www.particleincell.com/2012/bezier-splines/ and
  https://www.jacos.nl/jacos_html/spline/circular/index.html.
  
If I forget some borrowed code in the list above, just tell me.

Enjoy Gambas!

--
Benot
