# SOME DESCRIPTIVE TITLE.
# Copyright (C) YEAR THE PACKAGE'S COPYRIGHT HOLDER
# This file is distributed under the same license as the PACKAGE package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
# Translators:
# Дмитрий Ошкало <dmitry.oshkalo@gmail.com>, 2019
# Kашицин Роман <calorus@gmail.com>, 2019
# Олег o1hk <o1h2k3@yandex.ru>, 2019
# AlexL <loginov.alex.valer@gmail.com>, 2019
#
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2019-05-23 07:08+0300\n"
"PO-Revision-Date: 2019-05-09 00:48+0000\n"
"Last-Translator: AlexL <loginov.alex.valer@gmail.com>, 2019\n"
"Language-Team: Russian (https://www.transifex.com/rus-open-source/teams/44267/ru/)\n"
"Language: ru\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=4; plural=(n%10==1 && n%100!=11 ? 0 : n%10>=2 && n%10<=4 && (n%100<12 || n%100>14) ? 1 : n%10==0 || (n%10>=5 && n%10<=9) || (n%100>=11 && n%100<=14)? 2 : 3);\n"

#: app/examples/Control/Wizard/.project:18
msgid "Wizard example"
msgstr "Пример мастера"

#: app/examples/Control/Wizard/.src/FMain.class:34
msgid ""
"Your order was submitted successfully.\n"
"The wizard will close now."
msgstr ""
"Ваш заказ был успешно отправлен.\n"
"Мастер сейчас закроется."

#: app/examples/Control/Wizard/.src/FMain.class:58
msgid ""
"You didn't enter your address.\n"
"Your order can't be submitted."
msgstr ""
"Вы не ввели свой адрес.\n"
"Ваш заказ не может быть отправлен."

#: app/examples/Control/Wizard/.src/FMain.form:12
msgid "Introduction"
msgstr "Введение"

#: app/examples/Control/Wizard/.src/FMain.form:16
msgid ""
"Most of the properties of this wizard are selected in the properties window, not by code. This is because for designing the wizard you need the properties window anyway. \n"
"<p>\n"
"<p>\n"
"<i>To see the properties window, you have to do save the project in your home directory so that it is not write protected anymore.</i>\n"
"<p>\n"
"To design the pages of the wizard (or to see the properties), just click in the IDE on the \"Next\" button.\n"
"<p>\n"
"The essential property of the wizard container is <i>Count</i>. This defines the number of steps the user will have to click through. On the last page the button \"Next\" changes automatically to \"OK\" to finish the task.\n"
"<p>\n"
"In some situations you might want to skip steps of the wizard. You find this in the code of the CheckBoxes in step 2.\n"
"<p>\n"
msgstr ""
"Большинство свойств этого мастера выбираются в окне свойств, а не по коду. Это потому, что для разработки мастера вам всё равно нужно окно свойств. \n"
"<p>\n"
"<p>\n"
"<i>Чтобы увидеть окно свойств, вам нужно сохранить проект в вашем домашнем каталоге, чтобы он больше не был защищён от записи.</i>\n"
"<p>\n"
"Чтобы создать страницы мастера (или просмотреть свойства), просто нажмите в среде IDE на кнопку «Далее».\n"
"<p>\n"
"Основным свойством контейнера мастера является <i>Счёт</i>. Это определяет количество шагов, которые пользователь должен будет пройти. На последней странице кнопка «Далее» автоматически меняется на «ОК» для завершения задачи.\n"
"<p>\n"
"В некоторых ситуациях вы можете пропустить шаги мастера. Вы найдёте это в коде чекбоксов (флажков) в шаге 2.\n"
"<p>\n"

#: app/examples/Control/Wizard/.src/FMain.form:19
msgid "Your order"
msgstr "Ваш заказ"

#: app/examples/Control/Wizard/.src/FMain.form:23
msgid "I order, according to the terms & conditions, the following items:"
msgstr "Я заказываю в соответствии с положения и условиями следующие пункты:"

#: app/examples/Control/Wizard/.src/FMain.form:28
msgid "Amount"
msgstr "Кол-во"

#: app/examples/Control/Wizard/.src/FMain.form:36
msgid "Article"
msgstr "Статья"

#: app/examples/Control/Wizard/.src/FMain.form:40
msgid "lines of code from gambas 3"
msgstr "строк кода из gambas 3"

#: app/examples/Control/Wizard/.src/FMain.form:47
msgid "kg of frozen shrimps"
msgstr "кг замороженных креветок"

#: app/examples/Control/Wizard/.src/FMain.form:54
msgid "image files of the Gambas logo"
msgstr "файлов изображений логотипа Gambas"

#: app/examples/Control/Wizard/.src/FMain.form:58
msgid "I want to read the terms and conditions"
msgstr "Я хочу прочитать положения и условия"

#: app/examples/Control/Wizard/.src/FMain.form:63
msgid "I want to save my order"
msgstr "Я хочу сохранить свой заказ"

#: app/examples/Control/Wizard/.src/FMain.form:67
msgid "Terms and Conditions"
msgstr "Условия и положения"

#: app/examples/Control/Wizard/.src/FMain.form:73
msgid "As this is only an example,<br>nothing will be ordered,<br>nothing will be saved,<br>and nothing will be sent."
msgstr "Поскольку это только пример,<br>ничего не будет заказано,<br>ничего не будет сохранено<br>и ничего не будет отправлено."

#: app/examples/Control/Wizard/.src/FMain.form:77
msgid "Your address"
msgstr "Ваш адрес"

#: app/examples/Control/Wizard/.src/FMain.form:80
msgid "Please enter your address here:"
msgstr "Пожалуйста, введите ваш адрес здесь:"

#: app/examples/Control/Wizard/.src/FMain.form:86
msgid "Save your order"
msgstr "Сохранить ваш заказ"

#: app/examples/Control/Wizard/.src/FMain.form:90
msgid "Where do you want to save your order?"
msgstr "Где вы хотите сохранить свой заказ?"

#: app/examples/Control/Wizard/.src/FMain.form:96
msgid "Send your order"
msgstr "Отправить ваш заказ"

#: app/examples/Control/Wizard/.src/FMain.form:102
msgid "The following items:"
msgstr "Следующие пункты:"

#: app/examples/Control/Wizard/.src/FMain.form:106
msgid "will be delivered to:"
msgstr "будет доставлено на:"

#: app/examples/Control/Wizard/.src/FMain.form:114
msgid "Your order is now ready to be sent. Please check if everything is correct."
msgstr "Ваш заказ готов к отправке. Пожалуйста, проверьте, всё ли правильно."

