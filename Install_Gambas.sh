#!/usr/bin/env bash

DEBUG=0
RUN=0
DETECT=0
Sudo="sudo "

Which() {
OIFS=$IFS; IFS=$':'; for s in $PATH; do [ -e "$s/$1" ] && [ ! -L "$s/$1" ] && ls --color=never "$(realpath $s/$1)" 2>/dev/null && break; done; IFS=$OIFS
}

[ -z $(Which sudo) ] && Sudo=""

if [ ! -z "$1" ]; then

  for s in "$1 $2 $3"; do
    if [ -z "$s" ]; then
      continue
    elif [ "${s,,}" = "i" ]; then
      RUN=1;
    elif [ "${s,,}" = "a" ]; then
      DETECT=1;
    else
      AUTO="${s,,}"
    fi
  done
fi

Clear() { [ $DEBUG -eq 0 ] && clear; }

Ask() {  read -er -p "$1"; }

ErrorOut() { echo -e "\nError! $1"; read -p "Press return to exit."; exit 1; }

ctrl_c() { echo -en "\n** Ctrl-C was pressed, exiting...";  sleep 1; exit; }

VitalCommand() { eval "$1 2>&1; ER=$?"; [ $ER -ne 0 ] && ErrorOut "$2"; }

#############################################################

SplitLines() { OIFS=$IFS;  IFS=$'\n'; read -d "ó" -a SPLIT <<< "$1"ó; IFS=$OIFS; }

DetectSystem() {
  echo $ID
  exit
}

GetBuilds() {  # list builds from ci file.
  CNT=-1
  BUILDS=()
  while [ $CNT -lt ${#SPLIT[@]} ]; do
    ((CNT++))
    TRIMED=$(echo ${SPLIT[$CNT]##*( )})
    [ -z "$TRIMED" ] && continue
    [[ "$TRIMED" = "build:"* ]] && BUILDS+=(${TRIMED#*:})
  done
}
#############################################################

ProcessList() {
  TIFS=$IFS
  IFS=$'\n'
  read -d "|" -a LARY <<< "$LIST|"
  CNT=0;
  while [ $CNT -lt ${#LARY[@]} ]; do
    if [[ "$COM" = "apt"* ]]; then
  UNINSTALLGB="$UNINSTALLGB ${LARY[$CNT]%%/*}"
elif [[ "$COM" = "dnf"* ]]; then
UNINSTALLGB="$UNINSTALLGB ${LARY[$CNT]%.*}"
elif [[ "$COM" = "zypper"* ]]; then
if [[ "$LARY[$CNT]" = "gambas"* ]]; then
  UNINSTALLGB="$UNINSTALLGB $LARY[$CNT]";
fi
else
UNINSTALLGB="$UNINSTALLGB ${LARY[$CNT]}"
fi
((CNT++))
done
IFS=$TIFS
}

#############################################################

CheckForRepoGambas() {

  Clear
  echo "checking if Gambas3 is previously installed with ${INSTALLCOM%% *}..."
  if [[ "$COM" = "apt"* ]]; then
    LIST=$(apt list --installed "gambas3*" 2>/dev/null|grep 'gambas'|awk {'print $1'})
  elif [[ "$COM" = "pacman"* ]]; then
    LIST=$(pacman -Qsq "gambas3*" 2>/dev/null)
  elif [[ "$COM" = "dnf"* ]]; then
    LIST=$(dnf  list -C 'gambas3*' 2>/dev/null|grep @fedora|awk '{print $1}')
  elif [[ "$COM" = "zypper"* ]]; then
    LIST=$(zypper se -i 'gambas3*'|awk '{print $3}' 2>/dev/null)
  elif [[ "$COM" = "apk"* ]]; then
  echo "apk not done"
fi
sleep 1

if [ -z "$LIST" ]; then
  echo "Gambas is not previously installed by a package manager."
  HASGAMBAS=0
  sleep 1
  return
fi
HASGAMBAS=1

ProcessList
echo -en "\n\e[5;7mWarning...\e[0m\n\nThere are ${#LARY[@]} Gambas3 packages previously installed by your package manager and need to be uninstalled before the 'make install' command is executed.
\nNote.Removal Of ALL package manager Gambas3 items will not happen until just before 'make install' and not now so you will still have Gambas3 installed should the compilation process fail.\nPress return to continue: "
read
}
#"############################################################

CheckUninstallGambas() {

  if [ -z "$UNINSTALLGB" ]; then ISGONE=1; return ; fi
  echo "\nUninstalling previous repository version of Gambas3 now."
  ISGONE=1
  if [[ "$COM" = "apt"* ]]; then
    VitalCommand "$Sudo apt -y remove gambas3*"
  elif [[ "$COM" = "pacman"* ]]; then
    VitalCommand "$Sudo pacman -R --noconfirm $UNINSTALLGB"
  elif [[ "$COM" = "dnf"* ]]; then
    VitalCommand "$Sudo dnf remove -y $UNINSTALLGB"
  elif [[ "$COM" = "zypper"* ]]; then
    VitalCommand "$Sudo zypper remove -y $UNINSTALLGB"
  elif [[ "$COM" = "apk"* ]]; then
  echo "apk not done"
  ISGONE=0
else
  ISGONE=0
fi
}

#Main() Program starts here

SourceDir=$(pwd)

trap ctrl_c INT #make a Ctrl-C handler.

[ ! -e "$SourceDir/.gitlab-ci.yml" ] && SourceDir=${0%/*}

if [ ! -e "$SourceDir/.gitlab-ci.yml" ]; then
  for i in 1 2 3 4 5; do
    sleep 0.5
    [ -t 0 ] && [ -t 1 ] && break
    sleep 0.5
  done
  if [ -n $(Which "dialog") ]; then
    SourceDir=$(dialog --stdout --erase-on-exit --tab-correct --title "Select gambas source code directory" --fselect "$HOME/" -1 -1)
  else
    read -er -p "Enter source folder: " SourceDir
  fi
  [ ! -e "$SourceDir/.gitlab-ci.yml" ] && ErrorOut "No .gitlab-ci.yml file found in $SourceDir"
fi

CIFILE=$(cat "$SourceDir/.gitlab-ci.yml")
SplitLines "$CIFILE"
cd "$SourceDir"

GetBuilds

[ $DETECT -eq 1 ] && DetectSystem

if [ -z "$AUTO" ]; then
  echo -en "\033]2;'Gambas3 compiler script'\007"  # set terminal title text
  Ask "Show system release info?";

  if [[ "${REPLY,,}" = "y"* ]]; then
    if [ -e "/etc/upstream-release/lsb-release" ]; then
      cat "/etc/upstream-release/lsb-release"
    elif [ -e "/etc/lsb-release" ]; then
      cat "/etc/lsb-release"
    fi
    [ -e "/etc/os-release" ] && cat "/etc/os-release"
    echo
  fi

fi
echo
if [ -z "$AUTO" ]; then
  Ask "type I to install or return to just show the commands: ";
  [ "${REPLY,,}" = "i" ] && RUN=1
  echo "Reading $SourceDir $ANS"
fi

if [ -z "$AUTO" ]; then
  echo "Select your system by typing the number listed below... (press Ctrl-C to cancel)"
  select ANS in "${BUILDS[@]}"
  do

    if [ -n "$ANS" ]; then
      echo "This will install for $ANS ($REPLY)"
      [ $RUN -ne 1 ] && read -e -p "Press return to continue or Ctrl-C to cancel"
      echo
      break
    else
      echo "Answer not understood ($REPLY)"
      read -p "Press return."
      exit
    fi
  done

else
  ANS="$AUTO:"
fi

SplitLines "$CIFILE"

CNT=-1; INSIDE=0; INSTALLCOM=""

while [ $CNT -lt ${#SPLIT[@]} ]; do
  ((CNT++))
  TRIMED=$(echo ${SPLIT[$CNT]##*( )})
  [ -z "$TRIMED" ] && continue
  if [ $INSIDE -eq 1 ]; then
    if [[ "$TRIMED" = "build:"* ]] || [[ "$TRIMED" = "#"* ]]; then
      break
    else
      if [[ "$TRIMED" = "- GAMBAS_CONFIG_FAILURE"* ]]; then
      CONFIG=".${TRIMED#*.}"
      CONFIG=${CONFIG//.\/configure/.\/configure -q}
      continue
    elif [[ "$TRIMED" = "- "* ]] || [[ "$TRIMED" = *":"* ]]; then
      continue;
    fi
    INSTALLCOM="$INSTALLCOM$TRIMED "
  fi

else
  [[ "$TRIMED" = "build:$ANS" ]] && INSIDE=1
fi
done

[ -z "$INSTALLCOM" ] && ErrorOut "error getting data from ci file"

COM=${INSTALLCOM%% *}
INSTALLCOM=${INSTALLCOM//$COM/
$Sudo $COM}  # this line is attached by LF to the line above (dont move it)

if [ -z "$AUTO" ]; then
  CheckForRepoGambas
else
  HASGAMBAS=0
  ISGONE=1
fi

if [ $RUN -eq 1 ]; then
  eval "$INSTALLCOM"
  [ $? -ne 0 ] && ErrorOut "Error getting packages"
  ./reconf-all
  [ $? -ne 0 ] && ErrorOut "Error running ./reconf-all"
  eval "$CONFIG"   # ./configure line
  [ $? -ne 0 ] && ErrorOut "Error running ./configure"
  make -j$(nproc)
  [ $? -ne 0 ] && ErrorOut "Error running make"
  if [ -z "$AUTO" ]; then
    CheckUninstallGambas
  fi
  if [ "$HASGAMBAS" -eq 1  ]; then
    echo -n "$COM Package gambas was detected "
    [ "$ISGONE" -ne 1 ] && echo ", removal seemed succesfull" || echo -e ", removal seems \e[31munsuccesfull\e[0m"
  else
    echo  "$COM Package gambas was not detected"
  fi

  if [ -z "$AUTO" ]; then
    read -p "If you are sure $COM gambas is NOT installed then press return to continue to run 'make install'' or Ctrl-C to quit."; echo
  fi

  $Sudo make install
  [ $? -ne 0 ] && ErrorOut "Error running 'make install'"
else
  [ -z "$AUTO" ] && echo "# Copy the following commands to install gambas"
  echo "$INSTALLCOM"
  echo
  echo "./reconf-all"
  echo
  echo "$CONFIG"
  echo
  echo -e "make -j\$(nproc)"
  echo
  [ -z "$AUTO" ] && echo "# Uninstall packager installed gambas before running the next command."
  echo "$Sudo make install"
fi

if [ -z "$AUTO" ]; then
  echo
  read -p "# Finished, Press return to exit"
fi
