#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: gb.util 3.16.90\n"
"POT-Creation-Date: 2022-02-04 01:26 UTC\n"
"PO-Revision-Date: 2022-02-04 01:31 UTC\n"
"Last-Translator: Benoît Minisini <g4mba5@gmail.com>\n"
"Language: pl\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#: .project:1
msgid "Utility routines"
msgstr ""

#: File.class:22
msgid "&1 B"
msgstr ""

#: File.class:24
msgid "&1 KiB"
msgstr ""

#: File.class:26
msgid "&1 MiB"
msgstr ""

#: File.class:28
msgid "&1 GiB"
msgstr ""

#: File.class:34
msgid "&1 KB"
msgstr ""

#: File.class:36
msgid "&1 MB"
msgstr ""

#: File.class:38
msgid "&1 GB"
msgstr ""

#: Language.class:14
msgid "Afrikaans (South Africa)"
msgstr ""

#: Language.class:17
msgid "Arabic (Egypt)"
msgstr "Arabski (Egypt)"

#: Language.class:18
msgid "Arabic (Tunisia)"
msgstr "Arabski (Tunisia)"

#: Language.class:21
msgid "Azerbaijani (Azerbaijan)"
msgstr ""

#: Language.class:24
msgid "Bulgarian (Bulgaria)"
msgstr "Bułgarski (Bulgaria)"

#: Language.class:27
msgid "Catalan (Catalonia, Spain)"
msgstr "Kataloński (Catalonia, Spain)"

#: Language.class:31
msgid "Welsh (United Kingdom)"
msgstr "Walijski (United Kingdom)"

#: Language.class:34
msgid "Czech (Czech Republic)"
msgstr "Czeski (Czech Republic)"

#: Language.class:37
msgid "Danish (Denmark)"
msgstr "Duński (Denmark)"

#: Language.class:40
msgid "German (Germany)"
msgstr "Niemiecki (Germany)"

#: Language.class:41
msgid "German (Belgium)"
msgstr "Niemiecki (Belgium)"

#: Language.class:44
msgid "Greek (Greece)"
msgstr "Grecki (Greece)"

#: Language.class:47
msgid "English (common)"
msgstr "Angielski (ogólny)"

#: Language.class:48
msgid "English (United Kingdom)"
msgstr "Angielski (United Kingdom)"

#: Language.class:49
msgid "English (U.S.A.)"
msgstr "Angielski (U.S.A.)"

#: Language.class:50
msgid "English (Australia)"
msgstr "Angielski (Australia)"

#: Language.class:51
msgid "English (Canada)"
msgstr "Angielski (Canada)"

#: Language.class:54
msgid "Esperanto (Anywhere!)"
msgstr ""

#: Language.class:57
msgid "Spanish (common)"
msgstr ""

#: Language.class:58
msgid "Spanish (Spain)"
msgstr "Hiszpański (Spain)"

#: Language.class:59
msgid "Spanish (Argentina)"
msgstr "Hiszpański (Argentina)"

#: Language.class:62
msgid "Estonian (Estonia)"
msgstr "Estoński (Estonia)"

#: Language.class:65
msgid "Basque (Basque country)"
msgstr ""

#: Language.class:68
msgid "Farsi (Iran)"
msgstr ""

#: Language.class:71
msgid "Finnish (Finland)"
msgstr ""

#: Language.class:74
msgid "French (France)"
msgstr "Francuski (France)"

#: Language.class:75
msgid "French (Belgium)"
msgstr "Francuski (Belgium)"

#: Language.class:76
msgid "French (Canada)"
msgstr "Francuski (Canada)"

#: Language.class:77
msgid "French (Switzerland)"
msgstr "Francuski (Switzerland)"

#: Language.class:80
msgid "Galician (Spain)"
msgstr "Galicyjski (Spain)"

#: Language.class:83
msgid "Hebrew (Israel)"
msgstr "Hebrajski (Izrael)"

#: Language.class:86
msgid "Hindi (India)"
msgstr "Hindi (Indie)"

#: Language.class:89
msgid "Hungarian (Hungary)"
msgstr "Węgierski (Hungary)"

#: Language.class:92
msgid "Croatian (Croatia)"
msgstr "Chorwacki (Croatia)"

#: Language.class:95
msgid "Indonesian (Indonesia)"
msgstr "Indonezyjski (Indonesia)"

#: Language.class:98
msgid "Irish (Ireland)"
msgstr "Irlandzki (Ireland)"

#: Language.class:101
msgid "Icelandic (Iceland)"
msgstr ""

#: Language.class:104
msgid "Italian (Italy)"
msgstr "Włoski (Italy)"

#: Language.class:107
msgid "Japanese (Japan)"
msgstr "Japoński (Japonia)"

#: Language.class:110
msgid "Khmer (Cambodia)"
msgstr "Mon-Khmer (Kambodża)"

#: Language.class:113
msgid "Korean (Korea)"
msgstr ""

#: Language.class:116
msgid "Latin"
msgstr ""

#: Language.class:119
msgid "Lithuanian (Lithuania)"
msgstr ""

#: Language.class:122
msgid "Malayalam (India)"
msgstr "Malajalam (Indie)"

#: Language.class:125
msgid "Macedonian (Republic of Macedonia)"
msgstr ""

#: Language.class:128
msgid "Dutch (Netherlands)"
msgstr "Holenderski (Netherlands)"

#: Language.class:129
msgid "Dutch (Belgium)"
msgstr "Holenderski (Belgium)"

#: Language.class:132
msgid "Norwegian (Norway)"
msgstr "Norweski (Norway)"

#: Language.class:135
msgid "Punjabi (India)"
msgstr "Pendżabski (Indie)"

#: Language.class:138
msgid "Polish (Poland)"
msgstr "Polski (Poland)"

#: Language.class:141
msgid "Portuguese (Portugal)"
msgstr "Portugalski (Portugal)"

#: Language.class:142
msgid "Portuguese (Brazil)"
msgstr "Portugalski (Brazil)"

#: Language.class:145
msgid "Valencian (Valencian Community, Spain)"
msgstr ""

#: Language.class:148
msgid "Romanian (Romania)"
msgstr ""

#: Language.class:151
msgid "Russian (Russia)"
msgstr "Rosyjski (Russia)"

#: Language.class:154
msgid "Slovenian (Slovenia)"
msgstr "Słoweński (Slovenia)"

#: Language.class:157
msgid "Albanian (Albania)"
msgstr "Albański (Albania)"

#: Language.class:160
msgid "Serbian (Serbia & Montenegro)"
msgstr ""

#: Language.class:163
msgid "Swedish (Sweden)"
msgstr "Szwedzki (Sweden)"

#: Language.class:166
msgid "Turkish (Turkey)"
msgstr "Turecki (Turkey)"

#: Language.class:169
msgid "Ukrainian (Ukrain)"
msgstr ""

#: Language.class:172
msgid "Vietnamese (Vietnam)"
msgstr "Wietnamski (Wietnam)"

#: Language.class:175
msgid "Wallon (Belgium)"
msgstr "Waloński (Belgium)"

#: Language.class:178
msgid "Simplified chinese (China)"
msgstr "Uproszczony chiński (China)"

#: Language.class:179
msgid "Traditional chinese (Taiwan)"
msgstr "Tradycyjny chiński (Taiwan)"

#: Language.class:241
msgid "Unknown"
msgstr "Nieznany"
