#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: gb.form.mdi 3.10.90\n"
"POT-Creation-Date: 2024-05-04 13:49 UTC\n"
"PO-Revision-Date: 2018-03-17 10:03 UTC\n"
"Last-Translator: benoit <benoit@benoit-kubuntu>\n"
"Language: fr\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#: .project:1
msgid "Multiple document interface management"
msgstr "Interface de gestion multi document."

#: FMain.form:36 FMain2.form:35
msgid "Save"
msgstr ""

#: FMain.form:67
msgid "Hello"
msgstr ""

#: FMain.form:110
msgid "Border"
msgstr ""

#: FMain.form:115
msgid "Window"
msgstr ""

#: FMain.form:120
msgid "Orientation"
msgstr ""

#: FMain1.form:44
msgid "Help"
msgstr ""

#: FMain1.form:58
msgid "Button1"
msgstr ""

#: FMain1.form:79
msgid "Reparent"
msgstr ""

#: FMain2.form:25
msgid "Agnostic Scan Tool"
msgstr ""

#: FMain2.form:30
msgid "Menu1"
msgstr ""

#: FMain2.form:41
msgid "Save as"
msgstr ""

#: FMain2.form:50
msgid "Properties"
msgstr ""

#: FMain2.form:59
msgid "Quit"
msgstr ""

#: FMain2.form:95
msgid "Numériser Texte"
msgstr ""

#: FShortcut.class:65
msgid "Action"
msgstr "Action"

#: FShortcut.class:67
msgid "Shortcut"
msgstr "Raccourci"

#: FShortcut.class:178
msgid "Go back"
msgstr "Revenir"

#: FShortcut.class:178
msgid "You are going back to the default shortcuts."
msgstr "Vous allez revenir aux raccourcis initiaux."

#: FShortcut.class:264
msgid "Unable to export shortcut files."
msgstr "Impossible d'exporter les fichiers de raccourcis."

#: FShortcut.class:281
msgid "This file is not a Gambas shortcuts file."
msgstr "Ce fichier n'est pas un fichier de raccourcis Gambas."

#: FShortcut.class:305
msgid "Unable to import shortcut files."
msgstr "Impossible d'importer les fichiers de raccourcis."

#: FShortcut.class:313
msgid "Gambas shortcuts files"
msgstr "Fichiers de raccourcis Gambas"

#: FShortcut.class:314
msgid "Export shortcuts"
msgstr "Exporter les raccourcis"

#: FShortcut.class:327
msgid "Import shortcuts"
msgstr "Importer des raccourcis"

#: FShortcut.form:17
msgid "Configure shortcuts"
msgstr "Configurer les raccourcis"

#: FShortcut.form:42
msgid "Find shortcut"
msgstr "Trouver un raccourci"

#: FShortcut.form:49 FToolBarConfig.form:133
msgid "Reset"
msgstr "Réinitialiser"

#: FShortcut.form:55
msgid "Import"
msgstr "Importer"

#: FShortcut.form:61
msgid "Export"
msgstr "Exporter"

#: FShortcut.form:72
msgid "OK"
msgstr "OK"

#: FShortcut.form:78 FToolBarConfig.form:78
msgid "Cancel"
msgstr "Annuler"

#: FShortcutEditor.class:75
msgid "This shortcut is already used by the following action:"
msgstr "Ce raccourci est déjà utilisé par l'action suivante:"

#: FToolBar.class:1230
msgid "Configure &1 toolbar"
msgstr "Configurer la barre d'outils &1"

#: FToolBar.class:1232
msgid "Configure main toolbar"
msgstr "Configurer la barre d'outils principale"

#: FToolBarConfig.class:47
msgid "'&1' toolbar configuration"
msgstr "Configuration de la barre d'outils « &1 »"

#: FToolBarConfig.class:49
msgid "Toolbar configuration"
msgstr "Configuration de la barre d'outils"

#: FToolBarConfig.class:146
msgid "Separator"
msgstr "Séparateur"

#: FToolBarConfig.class:148
msgid "Expander"
msgstr "Extenseur"

#: FToolBarConfig.class:150
msgid "Space"
msgstr "Espace"

#: FToolBarConfig.class:393
msgid "Do you really want to reset the toolbar?"
msgstr "Désirez-vous vraiment réinitialiser la barre d'outils ?"

#: FToolBarConfig.form:36
msgid "Configure"
msgstr "Configurer"

#: FToolBarConfig.form:41
msgid "Icon size"
msgstr "Taille des icônes"

#: FToolBarConfig.form:44
msgid "Default"
msgstr ""

#: FToolBarConfig.form:49
msgid "Tiny"
msgstr "Minuscule"

#: FToolBarConfig.form:54
msgid "Small"
msgstr "Petite"

#: FToolBarConfig.form:59
msgid "Medium"
msgstr "Moyenne"

#: FToolBarConfig.form:64
msgid "Large"
msgstr "Grande"

#: FToolBarConfig.form:69
msgid "Huge"
msgstr "Enorme"

#: FToolBarConfig.form:111
msgid "Size"
msgstr "Taille"

#: FToolBarConfig.form:127
msgid "Undo"
msgstr "Annuler"

#: FToolBarConfig.form:139
msgid "Close"
msgstr "Fermer"

#: FWorkspace.form:36
msgid "Show"
msgstr "Afficher"

#: FWorkspace.form:41
msgid "Sort tabs"
msgstr "Trier les onglets"

#: FWorkspace.form:50
msgid "Close tabs on the right"
msgstr "Fermer les onglets à droite"

#: FWorkspace.form:55
msgid "Close other tabs"
msgstr "Fermer les autres onglets"

#: FWorkspace.form:60
msgid "Close all tabs"
msgstr "Fermer tous les onglets"

#: FWorkspace.form:67
msgid "Attach tab"
msgstr "Attacher l'onglet"

#: FWorkspace.form:71
msgid "Detach tab"
msgstr "Détacher l'onglet"

#: FWorkspace.form:76
msgid "Close tab"
msgstr "Fermer l'onglet"

#: FWorkspace.form:85
msgid "Previous tab"
msgstr "Onglet précédent"

#: FWorkspace.form:91
msgid "Next tab"
msgstr "Onglet suivant"

#, fuzzy
#~ msgid "Copy table"
#~ msgstr "Fermer l'onglet"

#, fuzzy
#~ msgid "Delete table"
#~ msgstr "Détacher l'onglet"

#, fuzzy
#~ msgid "New table"
#~ msgstr "Onglet suivant"

#, fuzzy
#~ msgid "Paste table"
#~ msgstr "Fermer l'onglet"
